package mx.rappi.movieapp.domain.MovieDetail

import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.module.RealmModule
import mx.rappi.movieapp.provider.MovieProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ShowMovieTest : TestCase(){

    private lateinit var movieProvider: MovieProvider
    private lateinit var showMovie: ShowMovie

    @Before
    fun init() {

        var realm = RealmModule()
        movieProvider = MovieProvider(realm.provideRealm(realm.provideRealmConfig()))
        showMovie = ShowMovie(movieProvider)

        var list : List<Movie> = listOf(
            Movie(
                id = 1,
                adult = true,
                backdropPath = "test",
                originalLanguage = "es",
                originalTitle = "test",
                overview = "test overview",
                popularity = 1.0,
                posterPath = "test",
                releaseDate = "test",
                title = "test",
                video = false,
                voteAverage = 1.0,
                voteCount = 1
            )
        )

        movieProvider.saveMovies(list, RealmModule.SEARCH)

    }

    @Test
    fun callWithData() = runBlocking<Unit> {

        val movie : Movie? = showMovie.call(1)
        delay(500)
        assertNotNull(movie)

    }

    @Test
    fun callWithOutData() = runBlocking<Unit> {

        val movie : Movie? = showMovie.call(0)
        delay(500)
        assertNull(movie)

    }

}