package mx.rappi.movieapp.domain.MovieList

import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mx.rappi.movieapp.api.ApiService
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.module.RealmModule
import mx.rappi.movieapp.network.MovieRepository
import mx.rappi.movieapp.provider.MovieProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GetPopularMoviesTest : TestCase() {

    private lateinit var apiService: ApiService
    private lateinit var movieProvider: MovieProvider
    private lateinit var movieRepository: MovieRepository
    private lateinit var getPopularMovies: GetPopularMovies

    @Before
    fun init() {

        var network = NetworkModule()
        var realm = RealmModule()
        movieProvider = MovieProvider(realm.provideRealm(realm.provideRealmConfig()))
        apiService = ApiService(network.provideApiClient(network.provideRetrofit(network.provideHttpClient(network.provideHttpLogging()))))
        movieRepository = MovieRepository(apiService, movieProvider)

        getPopularMovies = GetPopularMovies(movieRepository)
    }

    @Test
    fun callWithData() = runBlocking<Unit> {

        val list : List<Movie> ? = getPopularMovies.call()
        delay(500)

        assertTrue(list!!.size > 0)
        assertNotNull(list)

    }

}