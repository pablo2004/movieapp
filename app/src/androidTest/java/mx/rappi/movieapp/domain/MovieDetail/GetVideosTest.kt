package mx.rappi.movieapp.domain.MovieDetail

import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mx.rappi.movieapp.api.ApiService
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.module.RealmModule
import mx.rappi.movieapp.network.MovieRepository
import mx.rappi.movieapp.provider.MovieProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GetVideosTest : TestCase()  {

    private lateinit var apiService: ApiService
    private lateinit var movieProvider: MovieProvider
    private lateinit var movieRepository: MovieRepository
    private lateinit var getVideos: GetVideos

    @Before
    fun init() {

        var network = NetworkModule()
        var realm = RealmModule()
        movieProvider = MovieProvider(realm.provideRealm(realm.provideRealmConfig()))
        apiService = ApiService(network.provideApiClient(network.provideRetrofit(network.provideHttpClient(network.provideHttpLogging()))))
        movieRepository = MovieRepository(apiService, movieProvider)

        getVideos = GetVideos(movieRepository)
    }

    @Test
    fun callWithData() = runBlocking<Unit> {

        val list : List<Video> ? = getVideos.call("497698")
        delay(500)

        assertTrue(list!!.size > 0)
        assertNotNull(list)

    }

    @Test
    fun callWithOutData() = runBlocking<Unit> {

        val list : List<Video> ? = getVideos.call("0")
        delay(500)
        assertTrue(list!!.size == 0)
        assertNotNull(list)

    }

}