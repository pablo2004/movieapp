package mx.rappi.movieapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.realm.Realm
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.databinding.ItemMovieVerticalBinding
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.provider.GenreProvider
import javax.inject.Inject

class SerieVerticalAdapter @Inject constructor(private val genreProvider: GenreProvider) : RecyclerView.Adapter<SerieVerticalAdapter.SerieHolder> (){

    var list: List<Serie> = emptyList()
    var genres : MutableMap<Int, String> = mutableMapOf()
    var onClick : OnItemSelected? = null
    var onBottomListener : OnBottomListener? = null

    init {

        var genresList : List<Genre>? = genreProvider.getGenres()
        genresList?.let {
            genresList.forEach {
                genres.put(it.id, it.name)
            }
        }

    }

    interface OnItemSelected {
        fun onClick(item : Serie, view : View);
    }

    interface OnBottomListener {
        fun onBottom(position: Int);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerieHolder {
        val layout = ItemMovieVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SerieHolder(layout.root, onClick, genres)
    }

    override fun onBindViewHolder(holder: SerieHolder, position: Int) {
        holder.render(list[position])
        if (position == list.size-1){
            onBottomListener?.let {
                it.onBottom(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class SerieHolder (val view : View, val onClick : OnItemSelected?, val genres : MutableMap<Int, String>) : RecyclerView.ViewHolder(view){

        private val bindView: ItemMovieVerticalBinding = ItemMovieVerticalBinding.bind(view)

        fun render(serie: Serie){

            with(bindView){

                var genresData : MutableList<String> = mutableListOf()
                serie.genreIds.forEach {
                    genres.get(it)?.let {
                        genresData.add(it)
                    }
                }

                Glide.with(view.context).load(NetworkModule.IMAGE_PATH + serie.posterPath).into(bindView.ivImage)
                bindView.tvName.text = serie.name
                bindView.tvDescription.text = serie.overview
                bindView.tvGeneres.text = genresData.joinToString(", ")

                view.setOnClickListener({
                    onClick ?.let {
                        onClick.onClick(serie, view)
                    }
                })

            }


        }

    }

}