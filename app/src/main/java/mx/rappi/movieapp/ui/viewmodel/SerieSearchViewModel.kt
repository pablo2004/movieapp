package mx.rappi.movieapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.domain.SerieSearch.SearchSeries
import javax.inject.Inject

@HiltViewModel
class SerieSearchViewModel @Inject constructor(

    private val searchSeries: SearchSeries

): ViewModel (){

    val searchList = MutableLiveData<List<Serie>>()

    fun callSearchSeries(query : String){

        viewModelScope.launch {

            val result : List<Serie>? = searchSeries.call(query)

            result?.let {
                searchList.postValue(result)
            }

        }

    }

}