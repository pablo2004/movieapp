package mx.rappi.movieapp.ui.view

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import androidx.core.widget.addTextChangedListener
import mx.rappi.movieapp.core.ActivityCore
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.databinding.SerieSearchActivityBinding
import mx.rappi.movieapp.ui.adapters.SerieVerticalAdapter
import mx.rappi.movieapp.ui.viewmodel.SerieSearchViewModel

@AndroidEntryPoint
class SerieSearchActivity: ActivityCore() {

    @Inject lateinit var adpSearchSerie: SerieVerticalAdapter
    private lateinit var layout: SerieSearchActivityBinding
    private val serieSearchViewModel : SerieSearchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layout = SerieSearchActivityBinding.inflate(layoutInflater)
        setContentView(layout.root)

        serieSearchViewModel.searchList.observe(this, Observer { series ->

            if(series.size == 0){
                layout.rvSearch.visibility = View.GONE
                layout.tvEmpty.visibility = View.VISIBLE
            }
            else {
                layout.rvSearch.visibility = View.VISIBLE
                layout.tvEmpty.visibility = View.GONE
                loadSearchMovieAdapter(series)
            }

        })

        layout.etSearch.addTextChangedListener { editable ->
            var query = editable.toString()
            serieSearchViewModel.callSearchSeries(query)
        }

        layout.ivBack.setOnClickListener { onBackPressed() }
        layout.etSearch.requestFocus()

    }

    fun loadSearchMovieAdapter (series : List<Serie>){

        adpSearchSerie.list = series
        adpSearchSerie.onClick = object : SerieVerticalAdapter.OnItemSelected{
            override fun onClick(item: Serie, view: View) {

                var bundle = Bundle()
                bundle.putInt("id", item.id)
                SerieDetailActivity().launch(getCore(), bundle)

            }
        }

        layout.rvSearch.setHasFixedSize(true)
        layout.rvSearch.layoutManager = getLinearLayoutManagerVertical()
        layout.rvSearch.adapter = adpSearchSerie;

    }

}

