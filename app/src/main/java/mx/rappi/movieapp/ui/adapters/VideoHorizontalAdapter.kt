package mx.rappi.movieapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.databinding.ItemVideoBinding
import mx.rappi.movieapp.module.NetworkModule
import javax.inject.Inject

class VideoHorizontalAdapter @Inject constructor() : RecyclerView.Adapter<VideoHorizontalAdapter.VideoHolder> (){

    interface OnItemSelected {
        fun onClick(item : Video, view : View);
    }

    var list: List<Video> = emptyList()
    var onClick : OnItemSelected? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {
        val layout = ItemVideoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VideoHolder(layout.root, onClick)
    }

    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        holder.render(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class VideoHolder (val view : View, val onClick : OnItemSelected?) : RecyclerView.ViewHolder(view){

        private val bindView: ItemVideoBinding = ItemVideoBinding.bind(view)

        fun render(video: Video){

            Glide.with(view.context).load( NetworkModule.YOUTUBE_THUMB.replace("{YID}", video.key) ).into(bindView.ivImage)
            bindView.tvName.text = video.name

            view.setOnClickListener({
                if(onClick != null){
                    onClick.onClick(video, view)
                }
            })

        }

    }

}