package mx.rappi.movieapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.domain.MovieSearch.SearchMovies
import javax.inject.Inject

@HiltViewModel
class MovieSearchViewModel @Inject constructor(

    private val searchMovies : SearchMovies

): ViewModel (){

    val searchList = MutableLiveData<List<Movie>>()

    fun callSearchMovies(query : String){

        viewModelScope.launch {

            val result : List<Movie>? = searchMovies.call(query)

            result?.let {
                searchList.postValue(result)
            }

        }

    }

}