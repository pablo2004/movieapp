package mx.rappi.movieapp.ui.view

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import mx.rappi.movieapp.R
import mx.rappi.movieapp.ui.adapters.MovieHorizontalAdapter
import mx.rappi.movieapp.core.ActivityCore
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.databinding.MoviesActivityBinding
import mx.rappi.movieapp.ui.adapters.MovieVerticalAdapter
import mx.rappi.movieapp.ui.viewmodel.MovieListViewModel
import javax.inject.Inject
import com.synnapps.carouselview.ImageListener
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.databinding.SeriesActivityBinding
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.ui.adapters.SerieVerticalAdapter
import mx.rappi.movieapp.ui.viewmodel.SerieListViewModel

@AndroidEntryPoint
class SeriesActivity: ActivityCore() {

    @Inject lateinit var adpPopularSerie: SerieVerticalAdapter

    private var allSeries : MutableList<Serie> = mutableListOf()
    private var page : Int = 1;

    private lateinit var binding: SeriesActivityBinding

    private val serieListViewModel : SerieListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SeriesActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        serieListViewModel.topRatedList.observe(this, Observer { series ->
            loadCarouselView(series)
        })

        serieListViewModel.popularList.observe(this, Observer { series ->
            allSeries.addAll(series)
            loadPopularMovieAdapter(allSeries)
            binding.rvPopular.scrollToPosition((page-1) * 20)
        })

        serieListViewModel.genereList.observe(this, Observer { geners ->
            serieListViewModel.getPopularSeries( page.toString() )
            serieListViewModel.getTopRatedSeries()
        })

        binding.btnMovies.setOnClickListener {
            onBackPressed()
        }

        binding.btnSearch.setOnClickListener {
            SerieSearchActivity().launch(getCore())
        }

        serieListViewModel.getGenres()

    }

    fun loadPopularMovieAdapter (series : List<Serie>){

        adpPopularSerie.list = series
        adpPopularSerie.onClick = object : SerieVerticalAdapter.OnItemSelected{
            override fun onClick(item: Serie, view: View) {
                goToDetail(item.id)
            }
        }

        adpPopularSerie.onBottomListener = object : SerieVerticalAdapter.OnBottomListener {
            override fun onBottom(position: Int) {
                page = page + 1;
                serieListViewModel.getPopularSeries( page.toString() )
            }
        }

        binding.rvPopular.setHasFixedSize(true)
        binding.rvPopular.layoutManager = getLinearLayoutManagerVertical()
        binding.rvPopular.adapter = adpPopularSerie;

    }

    fun loadCarouselView(series : List<Serie>){

        val list : List<Serie>;

        if(series.size >= 5){
            list = series.subList(0, 5);
        }
        else {
            list = mutableListOf()
        }

        var imageListener: ImageListener = object : ImageListener {
            override fun setImageForPosition(position: Int, imageView: ImageView) {
                Glide.with(getCore()).load(NetworkModule.IMAGE_PATH + list[position].posterPath).into(imageView)
            }
        }

        val cvTopRated = binding.cvTopRated;
        cvTopRated.setImageListener(imageListener);
        cvTopRated.setPageCount(list.size);

        cvTopRated.setImageClickListener {
            goToDetail(list[it].id)
        }

    }

    fun goToDetail (id : Int){
        var bundle = Bundle()
        bundle.putInt("id", id)
        SerieDetailActivity().launch(getCore(), bundle)
    }

}

