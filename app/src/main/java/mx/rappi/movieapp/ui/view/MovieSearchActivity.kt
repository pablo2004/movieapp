package mx.rappi.movieapp.ui.view

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import mx.rappi.movieapp.ui.adapters.MovieVerticalAdapter
import javax.inject.Inject
import androidx.core.widget.addTextChangedListener
import mx.rappi.movieapp.core.ActivityCore
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.databinding.MovieSearchActivityBinding
import mx.rappi.movieapp.ui.viewmodel.MovieSearchViewModel

@AndroidEntryPoint
class MovieSearchActivity: ActivityCore() {

    @Inject lateinit var adpSearchMovie: MovieVerticalAdapter
    private lateinit var layout: MovieSearchActivityBinding
    private val movieSearchViewModel : MovieSearchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layout = MovieSearchActivityBinding.inflate(layoutInflater)
        setContentView(layout.root)

        movieSearchViewModel.searchList.observe(this, Observer { movies ->

            if(movies.size == 0){
                layout.rvSearch.visibility = View.GONE
                layout.tvEmpty.visibility = View.VISIBLE
            }
            else {
                layout.rvSearch.visibility = View.VISIBLE
                layout.tvEmpty.visibility = View.GONE
                loadSearchMovieAdapter(movies)
            }

        })

        layout.etSearch.addTextChangedListener { editable ->
            var query = editable.toString()
            movieSearchViewModel.callSearchMovies(query)
        }

        layout.ivBack.setOnClickListener { onBackPressed() }
        layout.etSearch.requestFocus()

    }

    fun loadSearchMovieAdapter (movies : List<Movie>){

        adpSearchMovie.list = movies
        adpSearchMovie.onClick = object : MovieVerticalAdapter.OnItemSelected{
            override fun onClick(item: Movie, view: View) {

                var bundle = Bundle()
                bundle.putInt("id", item.id)
                MovieDetailActivity().launch(getCore(), bundle)

            }
        }

        layout.rvSearch.setHasFixedSize(true)
        layout.rvSearch.layoutManager = getLinearLayoutManagerVertical()
        layout.rvSearch.adapter = adpSearchMovie;

    }

}

