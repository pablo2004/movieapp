package mx.rappi.movieapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.domain.GenderList.GetGenres
import mx.rappi.movieapp.domain.SerieList.GetPopularSeries
import mx.rappi.movieapp.domain.SerieList.GetTopRatedSerie
import javax.inject.Inject

@HiltViewModel
class SerieListViewModel @Inject constructor(

    private val getTopRatedSerie : GetTopRatedSerie,
    private val getPopularSeries: GetPopularSeries,
    private val getGenres: GetGenres

): ViewModel (){

    val topRatedList = MutableLiveData<List<Serie>>()

    val popularList = MutableLiveData<List<Serie>>()

    val genereList = MutableLiveData<List<Genre>>()

    fun getTopRatedSeries(){

        viewModelScope.launch {

            val result : List<Serie>? = getTopRatedSerie.call()

            result?.let {
                topRatedList.postValue(result)
            }

        }

    }

    fun getPopularSeries(page: String = "1"){

        viewModelScope.launch {

            val result : List<Serie>? = getPopularSeries.call(page)

            result?.let {
                popularList.postValue(result)
            }

        }

    }

    fun getGenres(){

        viewModelScope.launch {

            val result : List<Genre>? = getGenres.call()

            result?.let {
                genereList.postValue(result)
            }

        }

    }

}