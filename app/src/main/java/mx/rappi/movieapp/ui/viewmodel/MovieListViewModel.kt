package mx.rappi.movieapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.domain.GenderList.GetGenres
import mx.rappi.movieapp.domain.MovieList.GetTopRatedMovie
import mx.rappi.movieapp.domain.MovieList.GetPopularMovies
import javax.inject.Inject

@HiltViewModel
class MovieListViewModel @Inject constructor(

    private val getTopRatedMovie : GetTopRatedMovie,
    private val getPopularMovies: GetPopularMovies,
    private val getGenres: GetGenres

): ViewModel (){

    val topRatedList = MutableLiveData<List<Movie>>()

    val popularList = MutableLiveData<List<Movie>>()

    val genereList = MutableLiveData<List<Genre>>()

    fun getTopRatedMovies(){

        viewModelScope.launch {

            val result : List<Movie>? = getTopRatedMovie.call()

            result?.let {
                topRatedList.postValue(result)
            }

        }

    }

    fun getPopularMovies(page: String = "1"){

        viewModelScope.launch {

            val result : List<Movie>? = getPopularMovies.call(page)

            result?.let {
                popularList.postValue(result)
            }

        }

    }

    fun getGenres(){

        viewModelScope.launch {

            val result : List<Genre>? = getGenres.call()

            result?.let {
                genereList.postValue(result)
            }

        }

    }

}