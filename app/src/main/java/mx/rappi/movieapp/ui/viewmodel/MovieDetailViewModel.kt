package mx.rappi.movieapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.domain.GenderList.GenresFormat
import mx.rappi.movieapp.domain.MovieDetail.GetVideos
import mx.rappi.movieapp.domain.MovieDetail.ShowMovie
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(

    private val getVideos: GetVideos,
    private val showMovie : ShowMovie,
    private val genresFormat : GenresFormat

): ViewModel (){

    val movie = MutableLiveData<Movie>()
    val videos = MutableLiveData<List<Video>>()
    val video = MutableLiveData<Video>()
    val genres = MutableLiveData<String>()

    fun callMovie(id : Int){
        val _movie : Movie? = showMovie.call(id)
        _movie?.let {
            movie.postValue(_movie)
        }
    }

    fun getVideos(id : String){

        viewModelScope.launch {

            val result : List<Video>? = getVideos.call(id)

            result?.let {
                videos.postValue(result)
            }

        }

    }

    fun showVideo(vid : Video){
        video.postValue(vid)
    }

    fun getGenres(gens : String){

        viewModelScope.launch {

            val result : String? = genresFormat.call(gens)

            result?.let {
                genres.postValue(result)
            }

        }

    }

}