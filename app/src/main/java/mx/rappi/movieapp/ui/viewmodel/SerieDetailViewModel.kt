package mx.rappi.movieapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.domain.GenderList.GenresFormat
import mx.rappi.movieapp.domain.MovieDetail.ShowMovie
import mx.rappi.movieapp.domain.SerieDetail.GetSerieVideos
import mx.rappi.movieapp.domain.SerieDetail.ShowSerie
import javax.inject.Inject

@HiltViewModel
class SerieDetailViewModel @Inject constructor(

    private val getVideos: GetSerieVideos,
    private val showSerie: ShowSerie,
    private val genresFormat : GenresFormat

): ViewModel (){

    val videos = MutableLiveData<List<Video>>()
    val serie = MutableLiveData<Serie>()
    val video = MutableLiveData<Video>()
    val genres = MutableLiveData<String>()

    fun callSerie(id : Int){
        val _serie : Serie? = showSerie.call(id)
        _serie?.let {
            serie.postValue(_serie)
        }
    }

    fun getVideos(id : String){

        viewModelScope.launch {

            val result : List<Video>? = getVideos.call(id)

            result?.let {
                videos.postValue(result)
            }

        }

    }

    fun showVideo(vid : Video){
        video.postValue(vid)
    }

    fun getGenres(gens : String){

        viewModelScope.launch {

            val result : String? = genresFormat.call(gens)

            result?.let {
                genres.postValue(result)
            }

        }

    }

}