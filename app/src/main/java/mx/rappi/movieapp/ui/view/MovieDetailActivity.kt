package mx.rappi.movieapp.ui.view

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import mx.rappi.movieapp.core.ActivityCore
import mx.rappi.movieapp.databinding.MovieDetailActivityBinding
import mx.rappi.movieapp.ui.viewmodel.MovieDetailViewModel
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.ui.adapters.VideoHorizontalAdapter
import javax.inject.Inject

@AndroidEntryPoint
class MovieDetailActivity: ActivityCore() {

    @Inject
    lateinit var adpVideo: VideoHorizontalAdapter

    private lateinit var layout: MovieDetailActivityBinding
    private val movieDetailActivity : MovieDetailViewModel by viewModels()

    private var videoPlayer : YouTubePlayerView ? = null
    private var youTubePlayer : YouTubePlayer ? = null

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        layout = MovieDetailActivityBinding.inflate(layoutInflater)
        setContentView(layout.root)

        layout.ivBack.setOnClickListener { onBackPressed() }
        var id = getInt("id")

        movieDetailActivity.movie.observe(this, Observer { movie ->
            loadMovie(movie)
        })

        movieDetailActivity.videos.observe(this, Observer { videos ->
            loadVideoAdapter(videos)
        })

        movieDetailActivity.video.observe(this, Observer { video ->
            loadVideo(video)
        })

        movieDetailActivity.genres.observe(this, Observer { genres ->
            layout.tvGeneres.text = genres
        })

        if(id == 0){
            finish()
        }
        else {
            movieDetailActivity.callMovie(id)
            movieDetailActivity.getVideos(id.toString())
        }

        videoPlayer = layout.ypVideo

        videoPlayer?.let {

            videoPlayer!!.addYouTubePlayerListener(object: AbstractYouTubePlayerListener() {
                override fun onReady(ytp : YouTubePlayer) {
                    youTubePlayer = ytp
                }
            })

        }

    }

    fun loadMovie(movie : Movie){
        layout.tvName.text = movie.title
        layout.tvDescription.text = movie.overview
        movieDetailActivity.getGenres(movie.generes)
        Glide.with(getCore()).load(NetworkModule.IMAGE_PATH + movie.posterPath).into(layout.ivPoster)
    }

    fun loadVideo(video : Video){

        youTubePlayer?.let {
            videoPlayer?.visibility = View.VISIBLE
            youTubePlayer?.loadVideo(video.key, 0f)
        }

    }

    fun loadVideoAdapter (videos : List<Video>){

        adpVideo.list = videos
        adpVideo.onClick = object : VideoHorizontalAdapter.OnItemSelected{
            override fun onClick(item: Video, view: View) {
                movieDetailActivity.showVideo(item)
            }
        }

        layout.rvVideos.setHasFixedSize(true)
        layout.rvVideos.layoutManager = getLinearLayoutManagerHorizontal()
        layout.rvVideos.adapter = adpVideo;

    }

}

