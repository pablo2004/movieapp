package mx.rappi.movieapp.ui.view

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import mx.rappi.movieapp.R
import mx.rappi.movieapp.ui.adapters.MovieHorizontalAdapter
import mx.rappi.movieapp.core.ActivityCore
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.databinding.MoviesActivityBinding
import mx.rappi.movieapp.ui.adapters.MovieVerticalAdapter
import mx.rappi.movieapp.ui.viewmodel.MovieListViewModel
import javax.inject.Inject
import com.synnapps.carouselview.ImageListener
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.module.NetworkModule

@AndroidEntryPoint
class MoviesActivity: ActivityCore() {

    @Inject lateinit var adpTopRatedMovie: MovieHorizontalAdapter

    @Inject lateinit var adpPopularMovie: MovieVerticalAdapter

    private var allMovies : MutableList<Movie> = mutableListOf()
    private var page : Int = 1;

    private lateinit var binding: MoviesActivityBinding

    private val movieListViewModel : MovieListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MoviesActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        movieListViewModel.topRatedList.observe(this, Observer { movies ->
            loadCarouselView(movies)
        })

        movieListViewModel.popularList.observe(this, Observer { movies ->
            allMovies.addAll(movies)
            loadPopularMovieAdapter(allMovies)
            binding.rvPopular.scrollToPosition((page-1) * 20)
        })

        movieListViewModel.genereList.observe(this, Observer { geners ->
            movieListViewModel.getPopularMovies( page.toString() )
            movieListViewModel.getTopRatedMovies()
        })

        binding.btnSearch.setOnClickListener {
            MovieSearchActivity().launch(getCore())
        }

        binding.btnSeries.setOnClickListener {
            SeriesActivity().launch(getCore())
        }

        movieListViewModel.getGenres()

    }

    fun loadPopularMovieAdapter (movies : List<Movie>){

        adpPopularMovie.list = movies
        adpPopularMovie.onClick = object : MovieVerticalAdapter.OnItemSelected{
            override fun onClick(item: Movie, view: View) {
                goToDetail(item.id)
            }
        }

        adpPopularMovie.onBottomListener = object : MovieVerticalAdapter.OnBottomListener {
            override fun onBottom(position: Int) {
                page = page + 1;
                movieListViewModel.getPopularMovies( page.toString() )
            }
        }

        binding.rvPopular.setHasFixedSize(true)
        binding.rvPopular.layoutManager = getLinearLayoutManagerVertical()
        binding.rvPopular.adapter = adpPopularMovie;

    }

    fun loadCarouselView(movies : List<Movie>){

        val list : List<Movie>;

        if(movies.size >= 5){
            list = movies.subList(0, 5);
        }
        else {
            list = mutableListOf()
        }

        var imageListener: ImageListener = object : ImageListener {
            override fun setImageForPosition(position: Int, imageView: ImageView) {
                Glide.with(getCore()).load(NetworkModule.IMAGE_PATH + list[position].posterPath).into(imageView)
            }
        }

        val cvTopRated = binding.cvTopRated;
        cvTopRated.setImageListener(imageListener);
        cvTopRated.setPageCount(list.size);

        cvTopRated.setImageClickListener {
            goToDetail(list[it].id)
        }

    }

    fun goToDetail (id : Int){
        var bundle = Bundle()
        bundle.putInt("id", id)
        MovieDetailActivity().launch(getCore(), bundle)
    }

}

