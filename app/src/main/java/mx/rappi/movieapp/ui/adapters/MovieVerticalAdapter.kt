package mx.rappi.movieapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.realm.Realm
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.databinding.ItemMovieVerticalBinding
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.provider.GenreProvider
import javax.inject.Inject

class MovieVerticalAdapter @Inject constructor(private val genreProvider: GenreProvider) : RecyclerView.Adapter<MovieVerticalAdapter.MovieHolder> (){

    var list: List<Movie> = emptyList()
    var genres : MutableMap<Int, String> = mutableMapOf()
    var onClick : OnItemSelected? = null
    var onBottomListener : OnBottomListener? = null

    init {

        var genresList : List<Genre>? = genreProvider.getGenres()
        genresList?.let {
            genresList.forEach {
                genres.put(it.id, it.name)
            }
        }

    }

    interface OnItemSelected {
        fun onClick(item : Movie, view : View);
    }

    interface OnBottomListener {
        fun onBottom(position: Int);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val layout = ItemMovieVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieHolder(layout.root, onClick, genres)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.render(list[position])
        if (position == list.size-1){
            onBottomListener?.let {
                it.onBottom(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MovieHolder (val view : View, val onClick : OnItemSelected?, val genres : MutableMap<Int, String>) : RecyclerView.ViewHolder(view){

        private val bindView: ItemMovieVerticalBinding = ItemMovieVerticalBinding.bind(view)

        fun render(movie: Movie){

            with(bindView){

                var genresData : MutableList<String> = mutableListOf()
                movie.genreIds.forEach {
                    genres.get(it)?.let {
                        genresData.add(it)
                    }
                }

                Glide.with(view.context).load(NetworkModule.IMAGE_PATH + movie.posterPath).into(bindView.ivImage)
                bindView.tvName.text = movie.title
                bindView.tvDescription.text = movie.overview
                bindView.tvGeneres.text = genresData.joinToString(", ")

                view.setOnClickListener({
                    onClick ?.let {
                        onClick.onClick(movie, view)
                    }
                })

            }


        }

    }

}