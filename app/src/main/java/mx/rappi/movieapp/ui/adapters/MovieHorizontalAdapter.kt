package mx.rappi.movieapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.databinding.ItemMovieHorizontalBinding
import mx.rappi.movieapp.module.NetworkModule
import javax.inject.Inject

class MovieHorizontalAdapter @Inject constructor() : RecyclerView.Adapter<MovieHorizontalAdapter.MovieHolder> (){

    interface OnItemSelected {
        fun onClick(item : Movie, view : View);
    }

    var list: List<Movie> = emptyList()
    var onClick : OnItemSelected? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val layout = ItemMovieHorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieHolder(layout.root, onClick)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.render(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MovieHolder (val view : View, val onClick : OnItemSelected?) : RecyclerView.ViewHolder(view){

        private val bindView: ItemMovieHorizontalBinding = ItemMovieHorizontalBinding.bind(view)

        fun render(movie: Movie){

            with(bindView) {
                Glide.with(view.context).load(NetworkModule.IMAGE_PATH + movie.posterPath)
                    .into(bindView.ivImage)
                bindView.tvName.text = movie.title

                view.setOnClickListener({
                    if (onClick != null) {
                        onClick.onClick(movie, view)
                    }
                })
            }

        }

    }

}