package mx.rappi.movieapp.ui.view

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import mx.rappi.movieapp.core.ActivityCore
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.databinding.SerieDetailActivityBinding
import mx.rappi.movieapp.module.NetworkModule
import mx.rappi.movieapp.provider.GenreProvider
import mx.rappi.movieapp.ui.adapters.VideoHorizontalAdapter
import mx.rappi.movieapp.ui.viewmodel.SerieDetailViewModel
import javax.inject.Inject

@AndroidEntryPoint
class SerieDetailActivity: ActivityCore() {

    @Inject lateinit var adpVideo: VideoHorizontalAdapter

    private lateinit var layout: SerieDetailActivityBinding
    private val serieDetailActivity : SerieDetailViewModel by viewModels()

    private var videoPlayer : YouTubePlayerView ? = null
    private var youTubePlayer : YouTubePlayer ? = null

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        layout = SerieDetailActivityBinding.inflate(layoutInflater)
        setContentView(layout.root)

        layout.ivBack.setOnClickListener { onBackPressed() }
        var id = getInt("id")

        serieDetailActivity.serie.observe(this, Observer { serie ->
            loadSerie(serie)
        })

        serieDetailActivity.videos.observe(this, Observer { videos ->
            loadVideoAdapter(videos)
        })

        serieDetailActivity.video.observe(this, Observer { video ->
            loadVideo(video)
        })

        serieDetailActivity.genres.observe(this, Observer { genres ->
            layout.tvGeneres.text = genres
        })

        if(id == 0){
            finish()
        }
        else {
            serieDetailActivity.callSerie(id)
            serieDetailActivity.getVideos(id.toString())
        }

        videoPlayer = layout.ypVideo

        videoPlayer?.let {

            videoPlayer!!.addYouTubePlayerListener(object: AbstractYouTubePlayerListener() {
                override fun onReady(ytp : YouTubePlayer) {
                    youTubePlayer = ytp
                }
            })

        }

    }

    fun loadSerie(serie : Serie){
        layout.tvName.text = serie.name
        layout.tvDescription.text = serie.overview
        serieDetailActivity.getGenres(serie.generes)
        Glide.with(getCore()).load(NetworkModule.IMAGE_PATH + serie.posterPath).into(layout.ivPoster)
    }

    fun loadVideo(video : Video){

        youTubePlayer?.let {
            videoPlayer?.visibility = View.VISIBLE
            youTubePlayer?.loadVideo(video.key, 0f)
        }

    }

    fun loadVideoAdapter (videos : List<Video>){

        adpVideo.list = videos
        adpVideo.onClick = object : VideoHorizontalAdapter.OnItemSelected{
            override fun onClick(item: Video, view: View) {
                serieDetailActivity.showVideo(item)
            }
        }

        layout.rvVideos.setHasFixedSize(true)
        layout.rvVideos.layoutManager = getLinearLayoutManagerHorizontal()
        layout.rvVideos.adapter = adpVideo;

    }



}

