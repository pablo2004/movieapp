package mx.rappi.movieapp.api

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mx.rappi.movieapp.data.*
import mx.rappi.movieapp.data.movie.PopularResponse
import mx.rappi.movieapp.data.movie.SearchResponse
import mx.rappi.movieapp.data.movie.TopRatedResponse
import mx.rappi.movieapp.data.movie.VideoResponse
import mx.rappi.movieapp.data.tv.SearchTvResponse
import mx.rappi.movieapp.data.tv.TvPopularResponse
import mx.rappi.movieapp.data.tv.TvTopRatedResponse
import mx.rappi.movieapp.module.NetworkModule
import retrofit2.Response;
import java.net.UnknownHostException
import javax.inject.Inject

class ApiService @Inject constructor(
    private val apiClient : ApiClient,
)  {

    // MOVIES
    suspend fun callSearch(query: String) : SearchResponse {

        return withContext(Dispatchers.IO){

            var response : Response<SearchResponse> ? = null

            try {
                response = apiClient.search(NetworkModule.API_TOKEN, query)
                response!!.body() ?: SearchResponse()
            }
            catch (e : UnknownHostException){
                SearchResponse() !!
            }

        }

    }

    suspend fun callTopRated(page: String = "1") : TopRatedResponse {

        return withContext(Dispatchers.IO){

            var response : Response<TopRatedResponse> ? = null

            try {
                response = apiClient.topRated(NetworkModule.API_TOKEN, NetworkModule.LANGUAGE, page)
                response!!.body() ?: TopRatedResponse()
            }
            catch (e : UnknownHostException){
                TopRatedResponse() !!
            }

        }

    }

    suspend fun callPopular(page: String = "1") : PopularResponse {

        return withContext(Dispatchers.IO){

            var response : Response<PopularResponse> ? = null

            try {
                response = apiClient.popular(NetworkModule.API_TOKEN, NetworkModule.LANGUAGE, page)
                response!!.body() ?: PopularResponse()
            }
            catch (e : UnknownHostException){
                PopularResponse() !!
            }

        }

    }

    suspend fun callVideos(id: String) : VideoResponse {

        return withContext(Dispatchers.IO){

            var response : Response<VideoResponse> ? = null

            try {
                response = apiClient.videos(id, NetworkModule.API_TOKEN, NetworkModule.LANGUAGE)
                response!!.body() ?: VideoResponse()
            }
            catch (e : UnknownHostException){
                VideoResponse() !!
            }

        }

    }

    // TV SHOW

    suspend fun callTvSearch(query: String) : SearchTvResponse {

        return withContext(Dispatchers.IO){

            var response : Response<SearchTvResponse> ? = null

            try {
                response = apiClient.searchTv(NetworkModule.API_TOKEN, query)
                response!!.body() ?: SearchTvResponse()
            }
            catch (e : UnknownHostException){
                SearchTvResponse() !!
            }

        }

    }

    suspend fun callTvTopRated(page: String = "1") : TvTopRatedResponse {

        return withContext(Dispatchers.IO){

            var response : Response<TvTopRatedResponse> ? = null

            try {
                response = apiClient.tvTopRated(NetworkModule.API_TOKEN, NetworkModule.LANGUAGE, page)
                response!!.body() ?: TvTopRatedResponse()
            }
            catch (e : UnknownHostException){
                TvTopRatedResponse() !!
            }

        }

    }

    suspend fun callTvPopular(page: String = "1") : TvPopularResponse {

        return withContext(Dispatchers.IO){

            var response : Response<TvPopularResponse> ? = null

            try {
                response = apiClient.tvPopular(NetworkModule.API_TOKEN, NetworkModule.LANGUAGE, page)
                response!!.body() ?: TvPopularResponse()
            }
            catch (e : UnknownHostException){
                TvPopularResponse() !!
            }

        }

    }

    suspend fun callTvVideos(id: String) : VideoResponse {

        return withContext(Dispatchers.IO){

            var response : Response<VideoResponse> ? = null

            try {
                response = apiClient.tvVideos(id, NetworkModule.API_TOKEN, NetworkModule.LANGUAGE)
                response!!.body() ?: VideoResponse()
            }
            catch (e : UnknownHostException){
                VideoResponse() !!
            }

        }

    }
    // EXTRA

    suspend fun callGenres() : GenresResponse {

        return withContext(Dispatchers.IO){

            var response : Response<GenresResponse> ? = null

            try {
                response = apiClient.generes(NetworkModule.API_TOKEN, NetworkModule.LANGUAGE)
                response!!.body() ?: GenresResponse()
            }
            catch (e : UnknownHostException){
                GenresResponse() !!
            }

        }

    }

}