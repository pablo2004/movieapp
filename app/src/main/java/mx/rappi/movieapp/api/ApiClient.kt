package mx.rappi.movieapp.api

import mx.rappi.movieapp.data.*
import mx.rappi.movieapp.data.movie.PopularResponse
import mx.rappi.movieapp.data.movie.SearchResponse
import mx.rappi.movieapp.data.movie.TopRatedResponse
import mx.rappi.movieapp.data.movie.VideoResponse
import mx.rappi.movieapp.data.tv.SearchTvResponse
import mx.rappi.movieapp.data.tv.TvPopularResponse
import mx.rappi.movieapp.data.tv.TvTopRatedResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiClient {

    @GET("genre/movie/list")
    suspend fun generes( @Query("api_key") api_key : String, @Query("language") language : String) : Response<GenresResponse>

    // MOVIE

    @GET("search/movie")
    suspend fun search(@Query("api_key") api_key : String, @Query("query") query : String) : Response<SearchResponse>

    @GET("movie/top_rated")
    suspend fun topRated(@Query("api_key") api_key : String, @Query("language") language : String, @Query("page") page : String) : Response<TopRatedResponse>

    @GET("movie/popular")
    suspend fun popular(@Query("api_key") api_key : String, @Query("language") language : String, @Query("page") page : String) : Response<PopularResponse>

    @GET("movie/{ID}/videos")
    suspend fun videos(@Path("ID") id : String, @Query("api_key") api_key : String, @Query("language") language : String) : Response<VideoResponse>

    // SERIE

    @GET("search/tv")
    suspend fun searchTv(@Query("api_key") api_key : String, @Query("query") query : String) : Response<SearchTvResponse>

    @GET("tv/top_rated")
    suspend fun tvTopRated(@Query("api_key") api_key : String, @Query("language") language : String, @Query("page") page : String) : Response<TvTopRatedResponse>

    @GET("tv/popular")
    suspend fun tvPopular(@Query("api_key") api_key : String, @Query("language") language : String, @Query("page") page : String) : Response<TvPopularResponse>

    @GET("tv/{ID}/videos")
    suspend fun tvVideos(@Path("ID") id : String, @Query("api_key") api_key : String, @Query("language") language : String) : Response<VideoResponse>

}