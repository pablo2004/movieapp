package mx.rappi.movieapp.domain.MovieList

import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.network.MovieRepository
import javax.inject.Inject

class GetTopRatedMovie @Inject constructor(
    private val repository: MovieRepository
)
{

    suspend fun call():List<Movie>? {
        return repository.topRatedMovies()
    }

}