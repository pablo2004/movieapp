package mx.rappi.movieapp.domain.SerieList

import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.network.SerieRepository
import javax.inject.Inject

class GetTopRatedSerie @Inject constructor(
    private val repository: SerieRepository
)
{

    suspend fun call():List<Serie>? {
        return repository.topRatedTv()
    }

}