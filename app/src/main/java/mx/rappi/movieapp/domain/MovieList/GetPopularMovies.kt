package mx.rappi.movieapp.domain.MovieList

import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.network.MovieRepository
import javax.inject.Inject

class GetPopularMovies @Inject constructor(
    private val repository: MovieRepository
)
{

    suspend fun call(page: String = "1"):List<Movie>? {
        return repository.popularMovies(page)
    }

}