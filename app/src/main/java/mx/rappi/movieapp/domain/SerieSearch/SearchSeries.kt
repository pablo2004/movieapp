package mx.rappi.movieapp.domain.SerieSearch

import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.network.SerieRepository
import javax.inject.Inject

class SearchSeries @Inject constructor(
    private val repository: SerieRepository
) {

   suspend fun call(query : String):List<Serie>? {
        return repository.searchSeries(query)
   }

}