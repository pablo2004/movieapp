package mx.rappi.movieapp.domain.SerieDetail

import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.provider.SerieProvider
import javax.inject.Inject

class ShowSerie @Inject constructor(
    private val serieProvider: SerieProvider
)
{

    fun call(id : Int): Serie? {
        return serieProvider.getSerie(id)
    }

}