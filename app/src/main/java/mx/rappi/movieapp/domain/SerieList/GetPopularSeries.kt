package mx.rappi.movieapp.domain.SerieList

import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.network.SerieRepository
import javax.inject.Inject

class GetPopularSeries @Inject constructor(
    private val repository: SerieRepository
)
{

    suspend fun call(page: String = "1"):List<Serie>? {
        return repository.popularTv(page)
    }

}