package mx.rappi.movieapp.domain.GenderList

import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.network.MovieRepository
import javax.inject.Inject

class GetGenres @Inject constructor(
    private val repository: MovieRepository
)
{

    suspend fun call():List<Genre>? {
        return repository.genres()
    }

}