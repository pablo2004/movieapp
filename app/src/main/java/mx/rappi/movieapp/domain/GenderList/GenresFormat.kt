package mx.rappi.movieapp.domain.GenderList

import android.util.Log
import mx.rappi.movieapp.provider.GenreProvider
import javax.inject.Inject

class GenresFormat @Inject constructor(
    val genreProvider: GenreProvider
)
{

    fun call (parser : String): String {

        var result : String = "";

        if(parser != null && parser.contains(",")){

            var gens : List<String> = parser.split(",")
            var genres : MutableMap<Int, String> = mutableMapOf()

            genreProvider.getGenres().let {
                it?.forEach {
                    genres.put(it.id, it.name)
                }
            }

            var genresData : MutableList<String> = mutableListOf()
            gens.forEach {
                Log.e("Data", it.toString() + "");
                genres.get( it.toInt() )?.let {
                    genresData.add(it)
                }
            }

            result = genresData.joinToString(", ");

        }

        return result;
    }

}