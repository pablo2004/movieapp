package mx.rappi.movieapp.domain.SerieDetail

import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.network.SerieRepository
import javax.inject.Inject

class GetSerieVideos @Inject constructor(
    private val repository: SerieRepository
)
{

    suspend fun call(id : String): List<Video> {
        return repository.videosTv(id)
    }

}