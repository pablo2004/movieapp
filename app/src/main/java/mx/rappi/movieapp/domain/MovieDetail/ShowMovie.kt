package mx.rappi.movieapp.domain.MovieDetail

import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.provider.MovieProvider
import javax.inject.Inject

class ShowMovie @Inject constructor(
    private val movieProvider: MovieProvider
)
{

    fun call(id : Int): Movie? {
        return movieProvider.getMovie(id)
    }

}