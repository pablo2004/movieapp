package mx.rappi.movieapp.domain.MovieDetail

import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.network.MovieRepository
import javax.inject.Inject

class GetVideos @Inject constructor(
    private val repository: MovieRepository
)
{

    suspend fun call(id : String): List<Video> {
        return repository.videos(id)
    }

}