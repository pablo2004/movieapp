package mx.rappi.movieapp.domain.MovieSearch

import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.network.MovieRepository
import javax.inject.Inject

class SearchMovies @Inject constructor(
    private val repository: MovieRepository
) {

   suspend fun call(query : String):List<Movie>? {
        return repository.searchMovies(query)
   }

}