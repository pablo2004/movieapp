package mx.rappi.movieapp.network

import mx.rappi.movieapp.api.ApiService
import mx.rappi.movieapp.data.*
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.data.movie.PopularResponse
import mx.rappi.movieapp.data.movie.SearchResponse
import mx.rappi.movieapp.data.movie.TopRatedResponse
import mx.rappi.movieapp.data.movie.VideoResponse
import mx.rappi.movieapp.module.RealmModule
import mx.rappi.movieapp.provider.GenreProvider
import mx.rappi.movieapp.provider.MovieProvider
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val api : ApiService,
    private val movieProvider : MovieProvider,
    private val genreProvider: GenreProvider
) {

    suspend fun searchMovies(query : String) : List<Movie> {

        var result : List<Movie>
        val response : SearchResponse = api.callSearch(query)

        if(response.results.size == 0){
            result = movieProvider.getMovies(query, type = RealmModule.SEARCH)
        }
        else {
            result = response.results
            movieProvider.saveMovies(result, RealmModule.SEARCH)
        }

        return result;
    }

    suspend fun topRatedMovies() : List<Movie> {

        var result : List<Movie>
        val response : TopRatedResponse = api.callTopRated()

        if(response.results.size == 0){
            result = movieProvider.getMovies(type = RealmModule.TOP_RATED)
        }
        else {
            result = response.results
            movieProvider.saveMovies(result, RealmModule.TOP_RATED)
        }

        return result;
    }

    suspend fun popularMovies(page: String = "1") : List<Movie> {

        var result : List<Movie>
        val response : PopularResponse = api.callPopular(page)

        if(response.results.size == 0){
            result = movieProvider.getMovies(type = RealmModule.POPULAR)
        }
        else {
            result = response.results
            movieProvider.saveMovies(result, RealmModule.POPULAR)
        }

        return result;
    }

    suspend fun videos(id: String) : List<Video> {

        var result : List<Video>
        val response : VideoResponse = api.callVideos(id)
        result = response.results

        return result;
    }

    suspend fun genres() : List<Genre> {

        var result : List<Genre>
        val response : GenresResponse = api.callGenres()
        result = response.genres
        genreProvider.saveGeneres(result)

        return result;
    }

}