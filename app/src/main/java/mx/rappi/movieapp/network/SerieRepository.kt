package mx.rappi.movieapp.network

import mx.rappi.movieapp.api.ApiService
import mx.rappi.movieapp.data.*
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.data.model.Video
import mx.rappi.movieapp.data.movie.PopularResponse
import mx.rappi.movieapp.data.movie.TopRatedResponse
import mx.rappi.movieapp.data.movie.VideoResponse
import mx.rappi.movieapp.data.tv.SearchTvResponse
import mx.rappi.movieapp.data.tv.TvPopularResponse
import mx.rappi.movieapp.data.tv.TvTopRatedResponse
import mx.rappi.movieapp.module.RealmModule
import mx.rappi.movieapp.provider.SerieProvider
import javax.inject.Inject

class SerieRepository @Inject constructor(
    private val api : ApiService,
    private val serieProvider: SerieProvider,
) {

    suspend fun searchSeries(query : String) : List<Serie> {

        var result : List<Serie>
        val response : SearchTvResponse = api.callTvSearch(query)

        if(response.results.size == 0){
            result = serieProvider.getSeries(query, type = RealmModule.SEARCH)
        }
        else {
            result = response.results
            serieProvider.saveSeries(result, RealmModule.SEARCH)
        }

        return result;
    }

    suspend fun topRatedTv() : List<Serie> {

        var result : List<Serie>
        val response : TvTopRatedResponse = api.callTvTopRated()

        if(response.results.size == 0){
            result = serieProvider.getSeries(type = RealmModule.TOP_RATED)
        }
        else {
            result = response.results
            serieProvider.saveSeries(result, RealmModule.TOP_RATED)
        }

        return result;
    }

    suspend fun popularTv(page: String = "1") : List<Serie> {

        var result : List<Serie>
        val response : TvPopularResponse = api.callTvPopular(page)

        if(response.results.size == 0){
            result = serieProvider.getSeries(type = RealmModule.POPULAR)
        }
        else {
            result = response.results
            serieProvider.saveSeries(result, RealmModule.POPULAR)
        }

        return result;
    }

    suspend fun videosTv(id: String) : List<Video> {

        var result : List<Video>
        val response : VideoResponse = api.callTvVideos(id)
        result = response.results

        return result;
    }


}