package mx.rappi.movieapp.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import mx.rappi.movieapp.api.ApiClient
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.logging.HttpLoggingInterceptor

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    companion object {
        var BASE_URL = "https://api.themoviedb.org/3/"
        var API_TOKEN = "881b5ff063504c478ae6395883dad3a3"
        var LANGUAGE = "en-US"
        var IMAGE_PATH = "https://image.tmdb.org/t/p/w500"
        var YOUTUBE_THUMB = "https://img.youtube.com/vi/{YID}/0.jpg"
    }

    @Provides
    fun provideHttpLogging() : HttpLoggingInterceptor {
        val logger = HttpLoggingInterceptor()
        logger.setLevel(HttpLoggingInterceptor.Level.BODY)
        return logger
    }

    @Singleton
    @Provides
    fun provideHttpClient(logger : HttpLoggingInterceptor) : OkHttpClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(logger)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()

        return client
    }

    @Singleton
    @Provides
    fun provideRetrofit(client : OkHttpClient) : Retrofit {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit
    }

    @Singleton
    @Provides
    fun provideApiClient(retrofit: Retrofit) : ApiClient {
        return retrofit.create(ApiClient::class.java)
    }

}