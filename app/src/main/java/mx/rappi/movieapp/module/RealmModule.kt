package mx.rappi.movieapp.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

import io.realm.Realm
import io.realm.RealmConfiguration

@Module
@InstallIn(SingletonComponent::class)
class RealmModule {

    companion object {
        var TOP_RATED = 1
        var POPULAR = 2
        var SEARCH = 0
    }

    @Provides
    fun provideRealmConfig () : RealmConfiguration {
        val config = RealmConfiguration.Builder()
            .allowQueriesOnUiThread(true)
            .allowWritesOnUiThread(true)
            .deleteRealmIfMigrationNeeded()
            .build()
        return config;
    }

    @Provides
    fun provideRealm(config : RealmConfiguration) : Realm{
        return Realm.getInstance(config)
    }

}