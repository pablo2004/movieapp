package mx.rappi.movieapp.data.storage

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId

open class SerieLocal (

    @PrimaryKey
    var _id: ObjectId = ObjectId(),
    var id : Int = 0,
    var backdropPath : String ? = null,
    var originalLanguage : String ? = null,
    var originalName : String ? = null,
    var overview : String ? = null,
    var popularity : Double = 0.0,
    var posterPath : String ? = null,
    var firstAirDate : String ? = null,
    var name : String ? = null,
    var voteAverage : Double = 0.0,
    var voteCount : Int = 0,
    var type : Int = 0,
    var genres : String = ""

): RealmObject()