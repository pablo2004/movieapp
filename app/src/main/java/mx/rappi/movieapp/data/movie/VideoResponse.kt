package mx.rappi.movieapp.data.movie

import com.google.gson.annotations.SerializedName
import mx.rappi.movieapp.data.model.Video

data class VideoResponse (

    @SerializedName("id") var id : Int = 0,
    @SerializedName("results") var results : List<Video> = emptyList()

)