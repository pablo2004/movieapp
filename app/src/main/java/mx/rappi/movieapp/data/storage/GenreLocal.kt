package mx.rappi.movieapp.data.storage

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId

open class GenreLocal (

    @PrimaryKey
    var _id: ObjectId = ObjectId(),
    var id : Int = 0,
    var name : String ? = null,

): RealmObject()