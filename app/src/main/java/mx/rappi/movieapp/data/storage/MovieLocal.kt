package mx.rappi.movieapp.data.storage

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId

open class MovieLocal (

    @PrimaryKey
    var _id: ObjectId = ObjectId(),
    var id : Int = 0,
    var adult : Boolean = true,
    var backdropPath : String ? = null,
    var originalLanguage : String ? = null,
    var originalTitle : String ? = null,
    var overview : String ? = null,
    var popularity : Double = 0.0,
    var posterPath : String ? = null,
    var releaseDate : String ? = null,
    var title : String ? = null,
    var video : Boolean = false,
    var voteAverage : Double = 0.0,
    var voteCount : Int = 0,
    var type : Int = 0,
    var genres : String = ""

): RealmObject()