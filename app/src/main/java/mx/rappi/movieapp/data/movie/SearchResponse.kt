package mx.rappi.movieapp.data.movie

import com.google.gson.annotations.SerializedName
import mx.rappi.movieapp.data.model.Movie

data class SearchResponse (

    @SerializedName("page") var page : Int = 0,
    @SerializedName("results") var results : List<Movie> = emptyList(),
    @SerializedName("total_pages") var totalPages : Int = 0,
    @SerializedName("total_results") var totalResults : Int = 0

)