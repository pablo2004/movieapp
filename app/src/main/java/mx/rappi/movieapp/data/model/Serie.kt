package mx.rappi.movieapp.data.model

import com.google.gson.annotations.SerializedName

data class Serie (

    @SerializedName("backdrop_path") var backdropPath : String,
    @SerializedName("genre_ids") var genreIds : List<Int> = mutableListOf(),
    @SerializedName("origin_country") var originCountry : List<String> = mutableListOf(),
    @SerializedName("id") var id : Int,
    @SerializedName("original_language") var originalLanguage : String,
    @SerializedName("original_name") var originalName : String,
    @SerializedName("overview") var overview : String,
    @SerializedName("popularity") var popularity : Double,
    @SerializedName("poster_path") var posterPath : String,
    @SerializedName("first_air_date") var firstAirDate : String,
    @SerializedName("name") var name : String,
    @SerializedName("vote_average") var voteAverage : Double,
    @SerializedName("vote_count") var voteCount : Int,
    var generes : String = ""

)