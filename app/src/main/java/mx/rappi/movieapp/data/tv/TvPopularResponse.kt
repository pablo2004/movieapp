package mx.rappi.movieapp.data.tv

import com.google.gson.annotations.SerializedName
import mx.rappi.movieapp.data.model.Serie

data class TvPopularResponse (

    @SerializedName("page") var page : Int = 0,
    @SerializedName("results") var results : List<Serie> = emptyList(),
    @SerializedName("total_pages") var totalPages : Int = 0,
    @SerializedName("total_results") var totalResults : Int = 0

)