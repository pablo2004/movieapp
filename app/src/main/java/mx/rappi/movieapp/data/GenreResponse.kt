package mx.rappi.movieapp.data

import com.google.gson.annotations.SerializedName
import mx.rappi.movieapp.data.model.Genre

data class GenresResponse (

    @SerializedName("genres" ) var genres : List<Genre> = emptyList()

)