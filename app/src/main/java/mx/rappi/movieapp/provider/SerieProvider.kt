package mx.rappi.movieapp.provider

import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.model.Serie
import mx.rappi.movieapp.data.storage.MovieLocal
import mx.rappi.movieapp.data.storage.SerieLocal
import mx.rappi.movieapp.domain.GenderList.GenresFormat
import mx.rappi.movieapp.module.RealmModule
import javax.inject.Inject

class SerieProvider @Inject constructor(
    private val realm : Realm
) {

    fun getSerie (id : Int) : Serie? {

        var serie : Serie? = null

        if(!realm.isClosed){

            var it = realm.where(SerieLocal::class.java).equalTo("id", id).findFirst()

            it?.let {

                serie = Serie(
                    id = it.id,
                    backdropPath = it.backdropPath ?: "",
                    originalLanguage = it.originalLanguage ?: "",
                    originalName = it.originalName ?: "",
                    overview = it.overview ?: "",
                    popularity = it.popularity,
                    posterPath = it.posterPath ?: "",
                    firstAirDate = it.firstAirDate ?: "",
                    name = it.name ?: "",
                    voteAverage = it.voteAverage,
                    voteCount = it.voteCount,
                    generes = it.genres
                )

            }

        }

        return serie
    }

    fun getSeries(name : String = "", type : Int) : List<Serie> {

        val result : MutableList<Serie> = mutableListOf()

        if(!realm.isClosed){

            var query : RealmResults<SerieLocal>

            if(type == RealmModule.SEARCH){
                query = realm.where(SerieLocal::class.java).contains("name", name, Case.INSENSITIVE).findAll()
            }
            else {
                query = realm.where(SerieLocal::class.java).equalTo("type", type).findAll()
            }

            if(query != null && query.isValid() && query.size > 0){

                query.forEach { it ->

                    var serie = Serie(
                        id = it.id,
                        backdropPath = it.backdropPath ?: "",
                        originalLanguage = it.originalLanguage ?: "",
                        originalName = it.originalName ?: "",
                        overview = it.overview ?: "",
                        popularity = it.popularity,
                        posterPath = it.posterPath ?: "",
                        firstAirDate = it.firstAirDate ?: "",
                        name = it.name ?: "",
                        voteAverage = it.voteAverage,
                        voteCount = it.voteCount
                    )

                    result.add(serie)
                }

            }

        }

        return result

    }

    fun saveSeries(series : List<Serie>, type : Int){

        if(!realm.isClosed){

            realm.executeTransactionAsync { rlm: Realm ->

                series.forEach({

                    var local = SerieLocal()
                    local.id = it.id
                    local.backdropPath = it.backdropPath
                    local.originalLanguage = it.originalLanguage
                    local.originalName = it.originalName
                    local.overview = it.overview
                    local.popularity = it.popularity
                    local.posterPath = it.posterPath
                    local.firstAirDate = it.firstAirDate
                    local.name = it.name
                    local.voteAverage = it.voteAverage
                    local.voteCount = it.voteCount
                    local.type = type
                    local.genres = it.genreIds.joinToString ("," )

                    rlm.insertOrUpdate(local)

                })

            }

        }

    }

}