package mx.rappi.movieapp.provider

import io.realm.Realm
import mx.rappi.movieapp.data.model.Genre
import mx.rappi.movieapp.data.storage.GenreLocal
import javax.inject.Inject

class GenreProvider @Inject constructor(
    private val realm : Realm
) {

    fun getGenres () : List<Genre>? {

        var genres : MutableList<Genre> = mutableListOf()

        if(!realm.isClosed){

            var it = realm.where(GenreLocal::class.java).findAll()

            it?.let {

                it.forEach {
                    genres.add( Genre(id = it.id, name = it.name!! ) )
                }

            }

        }

        return genres
    }

    fun saveGeneres(generes : List<Genre>){

        if(!realm.isClosed){

            realm.executeTransactionAsync { rlm: Realm ->

                generes.forEach({

                    var local = GenreLocal()
                    local.id = it.id
                    local.name = it.name

                    rlm.insertOrUpdate(local)

                })

            }

        }

    }

}