package mx.rappi.movieapp.provider

import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import mx.rappi.movieapp.data.model.Movie
import mx.rappi.movieapp.data.storage.MovieLocal
import mx.rappi.movieapp.module.RealmModule
import javax.inject.Inject

class MovieProvider @Inject constructor(
    private val realm : Realm
) {

    fun getMovie (id : Int) : Movie? {

        var movie : Movie? = null

        if(!realm.isClosed){

            var it = realm.where(MovieLocal::class.java).equalTo("id", id).findFirst()

            it?.let {

                movie = Movie(
                    id = it.id,
                    adult = it.adult,
                    backdropPath = it.backdropPath ?: "",
                    originalLanguage = it.originalLanguage ?: "",
                    originalTitle = it.originalTitle ?: "",
                    overview = it.overview ?: "",
                    popularity = it.popularity,
                    posterPath = it.posterPath ?: "",
                    releaseDate = it.releaseDate ?: "",
                    title = it.title ?: "",
                    video = it.video,
                    voteAverage = it.voteAverage,
                    voteCount = it.voteCount,
                    generes = it.genres
                )

            }

        }

        return movie
    }

    fun getMovies(title : String = "", type : Int) : List<Movie> {

        val result : MutableList<Movie> = mutableListOf()

        if(!realm.isClosed){

            var query : RealmResults<MovieLocal>

            if(type == RealmModule.SEARCH){
                query = realm.where(MovieLocal::class.java).contains("title", title, Case.INSENSITIVE).findAll()
            }
            else {
                query = realm.where(MovieLocal::class.java).equalTo("type", type).findAll()
            }

            if(query != null && query.isValid() && query.size > 0){

                query.forEach { it ->

                    var movie = Movie(
                        id = it.id,
                        adult = it.adult,
                        backdropPath = it.backdropPath ?: "",
                        originalLanguage = it.originalLanguage ?: "",
                        originalTitle = it.originalTitle ?: "",
                        overview = it.overview ?: "",
                        popularity = it.popularity,
                        posterPath = it.posterPath ?: "",
                        releaseDate = it.releaseDate ?: "",
                        title = it.title ?: "",
                        video = it.video,
                        voteAverage = it.voteAverage,
                        voteCount = it.voteCount
                    )

                    result.add(movie)
                }

            }

        }

        return result

    }

    fun saveMovies(movies : List<Movie>, type : Int){

        if(!realm.isClosed){

            realm.executeTransactionAsync { rlm: Realm ->

                movies.forEach({

                    var local = MovieLocal()
                    local.id = it.id
                    local.adult = it.adult
                    local.backdropPath = it.backdropPath
                    local.originalLanguage = it.originalLanguage
                    local.originalTitle = it.originalTitle
                    local.overview = it.overview
                    local.popularity = it.popularity
                    local.posterPath = it.posterPath
                    local.releaseDate = it.releaseDate
                    local.title = it.title
                    local.video = it.video
                    local.voteAverage = it.voteAverage
                    local.voteCount = it.voteCount
                    local.type = type
                    local.genres = it.genreIds.joinToString ("," )

                    rlm.insertOrUpdate(local)

                })

            }

        }

    }

}