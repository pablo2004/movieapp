package mx.rappi.movieapp.core

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager


open class ActivityCore: AppCompatActivity() {

    lateinit var _core : ActivityCore;
    private lateinit var _handler : Handler

    fun getCore() : ActivityCore {
        return _core
    }

    fun getHandler() : Handler {
        return _handler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _core = this;
        _handler = Handler(Looper.getMainLooper())
    }

    fun launch(rootActivity : ActivityCore, bundle : Bundle? = null){
        val intent = Intent(rootActivity, this::class.java)
        if(bundle != null){
            intent.putExtra("bundle", bundle)
        }
        rootActivity.startActivity(intent, bundle)
    }

    fun getBundle() : Bundle {
        var bundle : Bundle ? = null;

        if(intent != null && intent.hasExtra("bundle")) {
            bundle = intent.getBundleExtra("bundle")
        }

        return bundle!!
    }

    fun getInt(key : String) : Int {
        var result : Int = 0

        getBundle().let {
            result = getBundle().getInt(key, 0)
        }

        return result
    }

    fun getString(key : String) : String {
        var result : String = ""

        if(getBundle() != null ){
            result = getBundle().getString(key, "")
        }

        return result
    }

    fun getLinearLayoutManagerHorizontal(): LinearLayoutManager? {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        return linearLayoutManager
    }

    fun getLinearLayoutManagerVertical(): LinearLayoutManager? {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        return linearLayoutManager
    }

}